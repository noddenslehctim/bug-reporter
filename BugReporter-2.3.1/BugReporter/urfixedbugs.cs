﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class urfixedbugs : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        int SignedInUserID = Login.SignedInUserID;
        
        int BugIDint = int.Parse(userbugs.BugID);

        public urfixedbugs()
        {
            InitializeComponent();

            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
        }

        private void displaydata()
        {
            String commandString = "SELECT * FROM Bugs Where ID = " + BugIDint;
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {   //added ID so that I can user can click on bug and edit it using the ID
                    label10.Text = (string)sqlconnreader["Author"];
                    label11.Text = (string)sqlconnreader["URL"];
                    label12.Text = (string)sqlconnreader["Filepath"];
                    label13.Text = (string)sqlconnreader["Project"];
                    label14.Text = (string)sqlconnreader["Class"];
                    label15.Text = (string)sqlconnreader["Method"];
                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }



        }

        public void insertRecord(String bugfix, String author, String desc, String code, String date, int UserID, int BugID, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);
                cmdInsert.Parameters.AddWithValue("@Fixed", bugfix);
                cmdInsert.Parameters.AddWithValue("@FixedAuthor", author);
                cmdInsert.Parameters.AddWithValue("@Description", desc);
                cmdInsert.Parameters.AddWithValue("@Code", code);
                cmdInsert.Parameters.AddWithValue("@Date", date);
                cmdInsert.Parameters.AddWithValue("@User_ID", UserID);
                cmdInsert.Parameters.AddWithValue("@Bug_ID", BugID);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(author.Text) ||
                string.IsNullOrEmpty(desc.Text) ||
                string.IsNullOrEmpty(code.Text) ||
                string.IsNullOrEmpty(date.Text))
            {
                MessageBox.Show("Error: Missing Details.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void cleartxtBoxes()
        {
            author.Text = desc.Text = code.Text = date.Text = "";
        }

        private void Fixed_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {


                String commandString = "UPDATE Bugs SET Fixed=@Fixed, FixedCode=@Code, FixedDesc=@Description, FixedDate=@Date, FixedAuth=@FixedAuthor, User_ID=@User_ID WHERE BugAuditId=@Bug_ID OR ID=@Bug_ID";

                insertRecord("Yes", author.Text, desc.Text, code.Text, date.Text, SignedInUserID, BugIDint, commandString);

                cleartxtBoxes();
                MessageBox.Show("Bug Added.");




            }
        }

        public void gotohomepage()
        {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage()
        {
            Application.Run(new homepage());
        }


        private void backbutton_Click(object sender, EventArgs e)
        {
            gotohomepage();
        }
    }
}
