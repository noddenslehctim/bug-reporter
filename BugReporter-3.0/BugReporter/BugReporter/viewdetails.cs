﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ColorCode;


namespace BugReporter
{
    public partial class viewdetails : Form
    {

        Thread thread;
        SqlCeConnection sqlconn;
        string BugID = auduithis.BugIDaudit;
        int BugIDint = int.Parse(auduithis.BugIDaudit); //change bugid to an int for the select statement - MS
        int SignedInUserID = Login.SignedInUserID;

        public viewdetails()
        {
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
        }


        private void displaydata()
        {
            String commandString = "SELECT * FROM Bugs Where ID = " + BugID;
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {   
                    //for coloured syntax
                    string sourceCode = (string)sqlconnreader["UpdateCode"];

                    string colorizedSourceCode = new CodeColorizer().Colorize(sourceCode, Languages.CSharp);

                    label10.Text = (string)sqlconnreader["UpdateAuth"];
                    label11.DocumentText  = colorizedSourceCode;
                    label12.Text = (string)sqlconnreader["UpdateDesc"];
                    label13.Text = (string)sqlconnreader["UpdateDate"];

                    

                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }



        }


        public void gotoauduithis()
        {
            this.Close();
            thread = new Thread(openauduithis);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openauduithis()
        {
            Application.Run(new auduithis());
        }
        private void backbutton_Click(object sender, EventArgs e)
        {
            gotoauduithis();
        }
    }
}
