﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlServerCe;
using BugReporter;

namespace BugReporterTest
{
    [TestClass]
    public class ReportTests
    {

        //This is a test to see if i can insert data
        //I am trying to insert a new bug into the database
        SqlCeConnection sqlconn;

        [TestMethod]
        public void Check_if_data_is_inserted()
        {

            //Inserts Data into the database.
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();

            string author = "unittest";
            string url = "unittest";
            string filepath = "unittest";
            string project = "unittest";
            string classtxt = "unittest";
            string method = "unittest";
            string desc = "unittest";
            string code = "unittest";
            string date = "unittest";
            int UserID = 2;
            string Fix = "No";



            String commandString = "INSERT INTO Bugs(Author, URL, Filepath, Project,Class, Method, Description, Code, Date, Fixed, User_ID) VALUES (@Author, @URL, @Filepath, @Project, @Class, @Method, @Description, @Code, @Date, @Fixed, @User_ID )";

            SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);
            cmdInsert.Parameters.AddWithValue("@Author", author);
            cmdInsert.Parameters.AddWithValue("@URL", url);
            cmdInsert.Parameters.AddWithValue("@Filepath", filepath);
            cmdInsert.Parameters.AddWithValue("@Project", project);
            cmdInsert.Parameters.AddWithValue("@Class", classtxt);
            cmdInsert.Parameters.AddWithValue("@Method", method);
            cmdInsert.Parameters.AddWithValue("@Description", desc);
            cmdInsert.Parameters.AddWithValue("@Code", code);
            cmdInsert.Parameters.AddWithValue("@Date", date);
            cmdInsert.Parameters.AddWithValue("@User_ID", UserID);
            cmdInsert.Parameters.AddWithValue("@Fixed", Fix);
            cmdInsert.ExecuteNonQuery();
        }
    }
}

