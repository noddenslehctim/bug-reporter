﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlServerCe;
using BugReporter;

namespace BugReporterTest
{
    [TestClass]
    public class FixBugsTests
    {
        //This is a test to see if i can retreive and update data
        //I am trying to find a bug entered into the database so that i can then update that bug

        SqlCeConnection sqlconn;

        //Doesn't Work - System.InvalidOperationsException: No data exists for the row/column
        /*
        [TestMethod]
        public void Check_if_data_is_displayed()
        {

            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();

            

            object expected = "63";

            String commandStringselect = "SELECT * FROM Bugs Where Fixed = 'No' AND BugAuditID IS NULL AND ID = @expected";

            SqlCeCommand checkdetails = new SqlCeCommand(commandStringselect, sqlconn);

            checkdetails.Parameters.AddWithValue("@expected", expected);

            SqlCeDataReader datareader = checkdetails.ExecuteReader();

            object actual = datareader["ID"];
            //Compares what its should get to what it does get displays error if they dont match
            Assert.AreEqual(expected, actual, "Username Not In Database");




        }*/

        [TestMethod]
        public void Check_if_data_is_inserted()
        {

            //Inserts Data into the database.
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();

            string author = "unittest";
            string desc = "unittest";
            string code = "unittest";
            string date = "unittest";
            int UserID = 2;
            string Fix = "Yes";
            int BugID = 63;



            String commandString = "UPDATE Bugs SET Fixed=@Fixed, UpdateCode=@Code, UpdateDesc=@Description, UpdateDate=@Date, UpdateAuth=@FixedAuthor, User_ID=@User_ID WHERE ID=@Bug_ID";

            SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);
            cmdInsert.Parameters.AddWithValue("@UpdateAuthor", author);
            cmdInsert.Parameters.AddWithValue("@Description", desc);
            cmdInsert.Parameters.AddWithValue("@Code", code);
            cmdInsert.Parameters.AddWithValue("@Date", date);
            cmdInsert.Parameters.AddWithValue("@Fixed", Fix);
            cmdInsert.Parameters.AddWithValue("@User_ID", UserID);
            cmdInsert.Parameters.AddWithValue("@Bug_ID", BugID);
        }
    }
}
