﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlServerCe;
using BugReporter;

namespace BugReporterTest
{
    [TestClass]
    public class AuditHisTests
    {
        SqlCeConnection sqlconn;

        [TestMethod]
        public void Check_if_data_is_displayed()
            //This is a test to see if i can retreive data
            //I am trying to find the first bug entered into the database
        {

            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();
            
            //Doesn't Work - System.InvalidOperationsException: No data exists for the row/column
            
            object expected = "63";

            String commandStringselect = "SELECT * FROM Bugs Where Fixed = 'No' AND BugAuditID = @expected";

            SqlCeCommand checkdetails = new SqlCeCommand(commandStringselect, sqlconn);

            checkdetails.Parameters.AddWithValue("@expected", expected);

            SqlCeDataReader datareader = checkdetails.ExecuteReader();

            object actual = datareader["ID"];

            //Compares what its should get to what it does get displays error if they dont match
            Assert.AreEqual(expected, actual, "Bugs Not In Database");
            



        }
    }
}
