﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlServerCe;
using BugReporter;

namespace BugReporterTest
{
    [TestClass]
    public class RegisterTests
    {

        //This is a test to see if i can insert data
        //I am trying to insert a new user into the user table and then search for the username in the database to check it was added
        SqlCeConnection sqlconn;

        [TestMethod]
        public void Check_if_data_is_inserted()
        {
            
                //Inserts Data into the database.
                sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
                sqlconn.Open();

                string insusername = "unittest";
                string inspassword = "unittest";

                      
                String commandString = "INSERT INTO Users(Username, Password) VALUES (@insusername, @inspassword)";

                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);
                cmdInsert.Parameters.AddWithValue("@insusername", insusername);
                cmdInsert.Parameters.AddWithValue("@inspassword", inspassword);
                cmdInsert.ExecuteNonQuery();

                //Doesn't Work
            /*
            object expected = "Mitchel";

            String commandStringselect = "SELECT * FROM Users  WHERE USERNAME = @expected";

            SqlCeCommand checkdetails = new SqlCeCommand(commandStringselect, sqlconn);

            checkdetails.Parameters.AddWithValue("@expected", expected);

            SqlCeDataReader datareader = checkdetails.ExecuteReader();

            object actual = datareader["Username"];
            
            //Compares what its should get to what it does get displays error if they dont match
            Assert.AreEqual(expected, actual, "Username Not In Database");
            */



        }
    }
}
