﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlServerCe;
using BugReporter;


namespace BugReporterTest
{
    [TestClass]
    public class LoginTests
    {

        //This is a test to see if i can retreive data
        //I am trying to find the username Mitchel in the User table so that i can login

        SqlCeConnection sqlconn;
        

        [TestMethod]
        public void Test_Username_is_in_database()
        {
            //Should check to see if the username "Mitchel" exists in the database, recieve System.Invalid.OperationException: No data exists for the row/column
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();
            
            object expected = "Mitchel";

            String commandString = "SELECT * FROM Users  WHERE Username = @expected";
            
            SqlCeCommand checkdetails = new SqlCeCommand(commandString, sqlconn);

            checkdetails.Parameters.AddWithValue("@expected", expected );

            SqlCeDataReader datareader = checkdetails.ExecuteReader();

            object actual = datareader["Username"];

            //Compares what its should get to what it does get displays error if they dont match
            Assert.AreEqual(expected, actual, "Username Not In Database");



        }
    }
}
