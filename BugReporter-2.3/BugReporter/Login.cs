﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlServerCe;

namespace BugReporter
{
    public partial class Login : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        public static int SignedInUserID; //Added Version 1.6 so that i can print the user id into the bug table - MS 


        public Login()
        {
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();

        }

        private void Login1_Click(object sender, EventArgs e)
        {
            if (checkInputs()) //Check the details entered into the textboxes - MS
            {
                String commandString = "SELECT * FROM Users  WHERE Username = @username AND Password = @password";
                SqlCeCommand checkdetails = new SqlCeCommand(commandString, sqlconn);

                checkdetails.Parameters.AddWithValue("@username", usernameBox.Text);
                checkdetails.Parameters.AddWithValue("@password", passBox.Text);

                SqlCeDataReader datareader = checkdetails.ExecuteReader();

                bool logbool = datareader.Read();

                if (logbool == true)//If the user logs in clear the text boxes and open the homepage form - MS
                {
                    SignedInUserID = (int)datareader["ID"]; //Added 1.6 saves the signed in users id - MS
                    cleartxtBoxes();
                    gotohomepage();
                }

                if (logbool == false)//If user enters incorrect details display error message - MS
                {
                    MessageBox.Show("Error: Incorrect Login Details Entered.");
                }



            }
        }

        private void Register1_Click(object sender, EventArgs e)
        {
            gotoregister();
        }
        
        public void gotohomepage() {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage(){
        Application.Run(new homepage());
        }

        public void gotoregister()
        {
            this.Close();
            thread = new Thread(openregister);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openregister()
        {
            Application.Run(new register());
        }

        public bool checkInputs() 
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(usernameBox.Text) ||
                string.IsNullOrEmpty(passBox.Text))
            {
                MessageBox.Show("Error: Login Details Incorrect.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void cleartxtBoxes() // Sets textboxes to empty - MS
        {
            usernameBox.Text = passBox.Text = "";
        }

    }
}
