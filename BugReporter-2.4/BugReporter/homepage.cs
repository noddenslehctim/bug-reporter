﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class homepage : Form
    {
        Thread thread;

        public homepage()
        {
            InitializeComponent();
        }

        private void repbugbutt_Click(object sender, EventArgs e)
        {
            gotoreport();
        }

        private void viewbugsbutt_Click(object sender, EventArgs e)
        {
            gotobugs();
        }

        private void urbugsbutt_Click(object sender, EventArgs e)
        {
            gotouserbugs();
        }

        public void gotoreport()
        {
            this.Close();
            thread = new Thread(openreport);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openreport()
        {
            Application.Run(new report());
        }

        public void gotobugs()
        {
            this.Close();
            thread = new Thread(openbugs);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openbugs()
        {
            Application.Run(new bugs());
        }

        public void gotouserbugs()
        {
            this.Close();
            thread = new Thread(openuserbugs);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openuserbugs()
        {
            Application.Run(new userbugs());
        }

        private void fixedbutt_Click(object sender, EventArgs e)
        {
            gotobugsfixed();
        }

        public void gotobugsfixed()
        {
            this.Close();
            thread = new Thread(openbugsfixed);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openbugsfixed()
        {
            Application.Run(new bugsfixed());
        }
    }
}
