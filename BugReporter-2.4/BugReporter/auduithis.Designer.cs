﻿namespace BugReporter
{
    partial class auduithis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateAuth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.detailsbutt = new System.Windows.Forms.Button();
            this.backbutt = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.UpdateAuth,
            this.UpdateDate,
            this.UpdateDesc,
            this.UpdateCode});
            this.dataGridView1.Location = new System.Drawing.Point(92, 90);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(543, 282);
            this.dataGridView1.TabIndex = 27;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // UpdateAuth
            // 
            this.UpdateAuth.HeaderText = "Update Author";
            this.UpdateAuth.Name = "UpdateAuth";
            // 
            // UpdateDate
            // 
            this.UpdateDate.HeaderText = "Date Updated";
            this.UpdateDate.Name = "UpdateDate";
            // 
            // UpdateDesc
            // 
            this.UpdateDesc.HeaderText = "Update Desc";
            this.UpdateDesc.Name = "UpdateDesc";
            // 
            // UpdateCode
            // 
            this.UpdateCode.HeaderText = "Update Code";
            this.UpdateCode.Name = "UpdateCode";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(229, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(326, 19);
            this.label5.TabIndex = 26;
            this.label5.Text = "Below are all the bugs assigned to the open bug.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(292, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 36);
            this.label1.TabIndex = 25;
            this.label1.Text = "Audit History";
            // 
            // detailsbutt
            // 
            this.detailsbutt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.detailsbutt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailsbutt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.detailsbutt.Location = new System.Drawing.Point(588, 400);
            this.detailsbutt.Name = "detailsbutt";
            this.detailsbutt.Size = new System.Drawing.Size(100, 40);
            this.detailsbutt.TabIndex = 30;
            this.detailsbutt.Text = "Details";
            this.detailsbutt.UseVisualStyleBackColor = false;
            this.detailsbutt.Click += new System.EventHandler(this.detailsbutt_Click);
            // 
            // backbutt
            // 
            this.backbutt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbutt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbutt.Location = new System.Drawing.Point(46, 400);
            this.backbutt.Name = "backbutt";
            this.backbutt.Size = new System.Drawing.Size(100, 40);
            this.backbutt.TabIndex = 29;
            this.backbutt.Text = "Back";
            this.backbutt.UseVisualStyleBackColor = false;
            this.backbutt.Click += new System.EventHandler(this.backbutt_Click);
            // 
            // auduithis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(734, 462);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.detailsbutt);
            this.Controls.Add(this.backbutt);
            this.Name = "auduithis";
            this.Text = "auduithis";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button detailsbutt;
        private System.Windows.Forms.Button backbutt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateAuth;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateCode;
    }
}