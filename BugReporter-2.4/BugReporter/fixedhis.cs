﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class fixedhis : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        string BugID = bugsfixed.BugID;
        int BugIDint = int.Parse(bugsfixed.BugID);
        public static string BugIDaudit;
        int SignedInUserID = Login.SignedInUserID;

        public fixedhis()
        {
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
        }

        private void displaydata()
        {
            String commandString = "SELECT * FROM Bugs Where Fixed = 'No' AND BugAuditID = " + BugIDint + " ORDER BY ID";
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {
                    this.dataGridView1.Rows.Add(sqlconnreader["ID"], sqlconnreader["UpdateAuth"], sqlconnreader["UpdateDate"], sqlconnreader["UpdateDesc"], sqlconnreader["UpdateCode"]);
                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }



        }

        public void gotofixeddetails()
        {
            this.Close();
            thread = new Thread(openfixeddetails);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openfixeddetails()
        {
            Application.Run(new fixeddetails());
        }
       

        private void detailsbutt_Click(object sender, EventArgs e)
        {
            gotofixeddetails();
        }

        public void gotobugsfixed()
        {
            this.Close();
            thread = new Thread(openbugsfixed);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openbugsfixed()
        {
            Application.Run(new bugsfixed());
        }

        private void backbutt_Click(object sender, EventArgs e)
        {
            gotobugsfixed();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BugIDaudit = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            MessageBox.Show("BugIDaudit = " + BugIDaudit);
        }
    }
}
