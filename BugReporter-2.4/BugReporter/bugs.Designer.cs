﻿namespace BugReporter
{
    partial class bugs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Author = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Project = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Class = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Method = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updatbugbutt = new System.Windows.Forms.Button();
            this.backbutt = new System.Windows.Forms.Button();
            this.fixbugbutt = new System.Windows.Forms.Button();
            this.audit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(275, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(194, 19);
            this.label5.TabIndex = 19;
            this.label5.Text = "Below are all the open bugs.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(332, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 36);
            this.label1.TabIndex = 18;
            this.label1.Text = "Bugs";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Author,
            this.Project,
            this.Class,
            this.Method,
            this.Description,
            this.Date});
            this.dataGridView1.Location = new System.Drawing.Point(47, 93);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(642, 282);
            this.dataGridView1.TabIndex = 20;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            // 
            // Author
            // 
            this.Author.HeaderText = "Author";
            this.Author.Name = "Author";
            // 
            // Project
            // 
            this.Project.HeaderText = "Project";
            this.Project.Name = "Project";
            // 
            // Class
            // 
            this.Class.HeaderText = "Class";
            this.Class.Name = "Class";
            // 
            // Method
            // 
            this.Method.HeaderText = "Method";
            this.Method.Name = "Method";
            // 
            // Description
            // 
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            // 
            // updatbugbutt
            // 
            this.updatbugbutt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.updatbugbutt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatbugbutt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.updatbugbutt.Location = new System.Drawing.Point(451, 398);
            this.updatbugbutt.Name = "updatbugbutt";
            this.updatbugbutt.Size = new System.Drawing.Size(100, 40);
            this.updatbugbutt.TabIndex = 21;
            this.updatbugbutt.Text = "Update Bug";
            this.updatbugbutt.UseVisualStyleBackColor = false;
            this.updatbugbutt.Click += new System.EventHandler(this.updatbugbutt_Click);
            // 
            // backbutt
            // 
            this.backbutt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.backbutt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backbutt.Location = new System.Drawing.Point(47, 398);
            this.backbutt.Name = "backbutt";
            this.backbutt.Size = new System.Drawing.Size(100, 40);
            this.backbutt.TabIndex = 22;
            this.backbutt.Text = "Back";
            this.backbutt.UseVisualStyleBackColor = false;
            this.backbutt.Click += new System.EventHandler(this.backbutt_Click);
            // 
            // fixbugbutt
            // 
            this.fixbugbutt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fixbugbutt.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixbugbutt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.fixbugbutt.Location = new System.Drawing.Point(589, 398);
            this.fixbugbutt.Name = "fixbugbutt";
            this.fixbugbutt.Size = new System.Drawing.Size(100, 40);
            this.fixbugbutt.TabIndex = 23;
            this.fixbugbutt.Text = "Fix Bug";
            this.fixbugbutt.UseVisualStyleBackColor = false;
            this.fixbugbutt.Click += new System.EventHandler(this.fixbugbutt_Click);
            // 
            // audit
            // 
            this.audit.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.audit.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.audit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.audit.Location = new System.Drawing.Point(321, 398);
            this.audit.Name = "audit";
            this.audit.Size = new System.Drawing.Size(100, 40);
            this.audit.TabIndex = 24;
            this.audit.Text = "Audit History";
            this.audit.UseVisualStyleBackColor = false;
            this.audit.Click += new System.EventHandler(this.audit_Click);
            // 
            // bugs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(734, 462);
            this.Controls.Add(this.audit);
            this.Controls.Add(this.fixbugbutt);
            this.Controls.Add(this.backbutt);
            this.Controls.Add(this.updatbugbutt);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "bugs";
            this.Text = "bugs";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button updatbugbutt;
        private System.Windows.Forms.Button backbutt;
        private System.Windows.Forms.Button fixbugbutt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Author;
        private System.Windows.Forms.DataGridViewTextBoxColumn Project;
        private System.Windows.Forms.DataGridViewTextBoxColumn Class;
        private System.Windows.Forms.DataGridViewTextBoxColumn Method;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.Button audit;
    }
}