﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class bugsfixed : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        public static string BugID;
        public bugsfixed()
        {
            
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
        }

        private void displaydata()
        {
            String commandString = "SELECT * FROM Bugs WHERE Fixed = 'Yes' AND BugAuditID IS NULL ORDER BY ID";
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {
                    this.dataGridView1.Rows.Add(sqlconnreader["ID"], sqlconnreader["Author"], sqlconnreader["Project"], sqlconnreader["Class"], sqlconnreader["Method"], sqlconnreader["Description"], sqlconnreader["Date"]);
                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }



        }

        public void gotohomepage()
        {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage()
        {
            Application.Run(new homepage());
        }

        private void backbutt_Click(object sender, EventArgs e)
        {
            gotohomepage();
        }

        public void gotofixedhis()
        {
            this.Close();
            thread = new Thread(openfixedhis);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openfixedhis()
        {
            Application.Run(new fixedhis());
        }

        private void audit_Click(object sender, EventArgs e)
        {
            gotofixedhis();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BugID = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            MessageBox.Show("BugID = " + BugID);
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FileStream fileStream = new FileStream(@"C:\Temp\export.txt", FileMode.Create);

            TextWriter sw = new StreamWriter(@"C:\Temp\Fixed_Bugs.txt");
            int rowcount = dataGridView1.Rows.Count;
            for (int i = 0; i < rowcount - 1; i++)
            {
                sw.WriteLine(dataGridView1.Rows[i].Cells[0].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[1].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[2].Value.ToString()+ dataGridView1.Rows[i].Cells[3].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[4].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[5].Value.ToString() + "\t" + dataGridView1.Rows[i].Cells[6].Value.ToString());
            }
            sw.Close();     //Don't Forget Close the TextWriter Object(sw)

            MessageBox.Show("Data Successfully Exported");

        }
    }
}
