﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class bugs : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        public static string BugID; //used for the Bug_ID - MS

        public bugs()
        {
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
            
        }

        private void displaydata() 
        {
            String commandString = "SELECT * FROM Bugs Where Fixed = 'No' ORDER BY ID";
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {   //added ID so that I can user can click on bug and edit it using the ID
                    this.dataGridView1.Rows.Add(sqlconnreader["ID"], sqlconnreader["Author"], sqlconnreader["Project"], sqlconnreader["Class"], sqlconnreader["Method"], sqlconnreader["Description"], sqlconnreader["Date"]);
                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }
        


            }

        private void backbutt_Click(object sender, EventArgs e)
        {
            gotohomepage();
        }

        private void updatbugbutt_Click(object sender, EventArgs e)
        {
            gotoupdatebug();
        }

        public void gotohomepage()
        {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage()
        {
            Application.Run(new homepage());
        }

        public void gotoupdatebug()
        {
            this.Close();
            thread = new Thread(openupdatebug);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openupdatebug()
        {
            Application.Run(new updatebug());
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BugID = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            MessageBox.Show("BugID = " + BugID);
        }

        public void gotofixbugs()
        {
            this.Close();
            thread = new Thread(openfixbugs);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openfixbugs()
        {
            Application.Run(new fixbugs());
        }

        private void fixbugbutt_Click(object sender, EventArgs e)
        {
            gotofixbugs();
        }
    }
}
