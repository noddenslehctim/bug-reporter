﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class userbugs : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        int SignedInUserID = Login.SignedInUserID;
        public static string BugID;

        public userbugs()
        {
            InitializeComponent();
            
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
        }

        private void displaydata()
        {
            String commandString = "SELECT * FROM Bugs WHERE User_ID = " + SignedInUserID + "ORDER BY ID";
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {
                    this.dataGridView1.Rows.Add(sqlconnreader["ID"], sqlconnreader["Author"], sqlconnreader["Project"], sqlconnreader["Class"], sqlconnreader["Method"], sqlconnreader["Description"], sqlconnreader["Date"]);
                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }



        }


        private void updatbugbutt_Click(object sender, EventArgs e)
        {
            gotoupdateurbugs();
        }

        public void gotoupdateurbugs()
        {
            this.Close();
            thread = new Thread(openupdateurbugs);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openupdateurbugs()
        {
            Application.Run(new updateurbugs());
        }

        public void gotohomepage()
        {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage()
        {
            Application.Run(new homepage());
        }

        private void backbutt_Click(object sender, EventArgs e)
        {
            gotohomepage();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BugID = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            MessageBox.Show("BugID = " + BugID);
        }
    }
}
