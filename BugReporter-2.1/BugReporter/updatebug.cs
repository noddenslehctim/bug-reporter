﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class updatebug : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        string BugID = bugs.BugID;
        int BugIDint = int.Parse(bugs.BugID); //change bugid to an int for the select statement - MS
        int SignedInUserID = Login.SignedInUserID;
        public updatebug()
        {
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf");
            sqlconn.Open();
            displaydata();
        }

        private void displaydata()
        {
            String commandString = "SELECT * FROM Bugs Where ID = " + BugID;
            SqlCeCommand displaydata = new SqlCeCommand(commandString, sqlconn);

            try
            {
                SqlCeDataReader sqlconnreader = displaydata.ExecuteReader();

                while (sqlconnreader.Read())
                {   //added ID so that I can user can click on bug and edit it using the ID
                    label10.Text=(string)sqlconnreader["Author"];
                    label11.Text = (string)sqlconnreader["URL"];
                    label12.Text = (string)sqlconnreader["Filepath"];
                    label13.Text = (string)sqlconnreader["Project"];
                    label14.Text = (string)sqlconnreader["Class"];
                    label15.Text = (string)sqlconnreader["Method"];
                }
            }

            catch (SqlCeException ex) { MessageBox.Show("Error"); }

            

        }

        public void insertRecord(String author, String desc, String code, String date, String Fix, int UserID, int BugID, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);

                cmdInsert.Parameters.AddWithValue("@UpdateAuthor", author);
                cmdInsert.Parameters.AddWithValue("@Description", desc);
                cmdInsert.Parameters.AddWithValue("@Code", code);
                cmdInsert.Parameters.AddWithValue("@Date", date);
                cmdInsert.Parameters.AddWithValue("@Fixed", Fix);
                cmdInsert.Parameters.AddWithValue("@User_ID", UserID);
                cmdInsert.Parameters.AddWithValue("@Bug_ID", BugID);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(author.Text) ||
                string.IsNullOrEmpty(desc.Text) ||
                string.IsNullOrEmpty(code.Text) ||
                string.IsNullOrEmpty(date.Text))
            {
                MessageBox.Show("Error: Missing Details.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void cleartxtBoxes()
        {
            author.Text =  desc.Text = code.Text = date.Text = "";
        }

        private void RepButton_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "INSERT INTO Bugs(UpdateAuth, UpdateDesc, UpdateCode, UpdateDate, Fixed, User_ID , BugAuditID) VALUES (@UpdateAuthor, @Description, @Code, @Date, @Fixed, @User_ID, @Bug_ID )";

                insertRecord(author.Text, desc.Text, code.Text, date.Text, "No", SignedInUserID, BugIDint, commandString);

                cleartxtBoxes();
                MessageBox.Show("Bug Added.");
       



            }
        }

        public void gotohomepage()
        {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage()
        {
            Application.Run(new homepage());
        }

        private void backbutton_Click(object sender, EventArgs e)
        {
            gotohomepage();
        }

    }

}
