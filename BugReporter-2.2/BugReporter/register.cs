﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlServerCe;

namespace BugReporter
{
    public partial class register : Form
    {
        Thread thread;
        SqlCeConnection sqlconn;
        bool usernameavailable = false;


        public register()
        {
            InitializeComponent();

            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();
        }


        public void insertRecord(String username, String password, String commandString)
        {

            try 
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);

                cmdInsert.Parameters.AddWithValue("@username", usernameBox.Text);
                cmdInsert.Parameters.AddWithValue("@password", passBox.Text); 
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void RegButton_Click(object sender, EventArgs e)
        {
            if (checkInputs() && checkpass())
  	      {

                String checkusername = "SELECT * FROM users Where Username = @username";
                SqlCeCommand sqlconn1 = new SqlCeCommand(checkusername, sqlconn);
                sqlconn1.Parameters.AddWithValue("@username",usernameBox.Text);
                SqlCeDataReader data = sqlconn1.ExecuteReader();
                bool usernameavailable = data.Read();

                if (usernameavailable == true)
                {
                    MessageBox.Show("Error:Username has already been taken, please enter a different one.");
                }

                if (usernameavailable == false)
                {
                    String commandString = "INSERT INTO Users(Username, Password) VALUES (@username, @password)";

                    insertRecord(usernameBox.Text, passBox.Text, commandString);

                    cleartxtBoxes();
                    gotologin();
                }
        	}

        }

        public bool checkpass() //checks passwords match each other - MS
        {
            bool checkpass1 = true;
            if (passBox.Text != repassbox.Text)
            {
                MessageBox.Show("Error: Passwords Incorrect.");
                checkpass1 = false;
            } return (checkpass1);
        }



        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(usernameBox.Text) ||
                string.IsNullOrEmpty(passBox.Text))
            {
                MessageBox.Show("Error: Missing Details.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }


        public void gotologin()
        {
            this.Close();
            thread = new Thread(openlogin);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openlogin()
        {
            Application.Run(new Login());
        }

        public void cleartxtBoxes()
        {
            usernameBox.Text = passBox.Text = repassbox.Text = "";
        }


    }
}
