﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlServerCe;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class report : Form
     {
        Thread thread;
        SqlCeConnection sqlconn;
        int SignedInUserID = Login.SignedInUserID;

        public report()
        {
            InitializeComponent();
            sqlconn = new SqlCeConnection(@"Data Source=C:\Temp\BugReporter.sdf"); //Connection to the database - MS
            sqlconn.Open();
        }

        public void insertRecord(String author, String url, String filepath, String project, String classtxt, String method, String desc, String code, String date, String Fix, int UserID, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, sqlconn);

                cmdInsert.Parameters.AddWithValue("@Author", author);
                cmdInsert.Parameters.AddWithValue("@URL", url); 
                cmdInsert.Parameters.AddWithValue("@Filepath", filepath); 
                cmdInsert.Parameters.AddWithValue("@Project", project); 
                cmdInsert.Parameters.AddWithValue("@Class", classtxt); 
                cmdInsert.Parameters.AddWithValue("@Method", method); 
                cmdInsert.Parameters.AddWithValue("@Description", desc);
                cmdInsert.Parameters.AddWithValue("@Code", code);
                cmdInsert.Parameters.AddWithValue("@Date", date);
                cmdInsert.Parameters.AddWithValue("@User_ID", UserID);
                cmdInsert.Parameters.AddWithValue("@Fixed", Fix); 
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(author.Text) ||
                string.IsNullOrEmpty(url.Text) ||
                string.IsNullOrEmpty(filepath.Text) ||
                string.IsNullOrEmpty(project.Text) ||
                string.IsNullOrEmpty(classtxt.Text) ||
                string.IsNullOrEmpty(method.Text) ||
                string.IsNullOrEmpty(desc.Text) ||
                string.IsNullOrEmpty(code.Text) ||
                string.IsNullOrEmpty(date.Text))

            {
                MessageBox.Show("Error: Missing Details.");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void cleartxtBoxes()
        {
            author.Text = url.Text =  filepath.Text = project.Text = classtxt.Text = method.Text = desc.Text = code.Text = date.Text = "";
        }

        public void gotohomepage()
        {
            this.Close();
            thread = new Thread(openhomepage);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void openhomepage()
        {
            Application.Run(new homepage());
        }

        private void RepButton_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                    String commandString = "INSERT INTO Bugs(Author, URL, Filepath, Project,Class, Method, Description, Code, Date, Fixed, User_ID) VALUES (@Author, @URL, @Filepath, @Project, @Class, @Method, @Description, @Code, @Date, @Fixed, @User_ID )";

                    insertRecord(author.Text, url.Text, filepath.Text, project.Text, classtxt.Text, method.Text, desc.Text, code.Text, date.Text, "No", SignedInUserID, commandString);
                    
                    cleartxtBoxes();
                    MessageBox.Show("Bug Added.");
                    

                   
                    
                }
            }



        private void backbutton_Click(object sender, EventArgs e)
        {
            gotohomepage();
        }
        }




    }

