﻿namespace BugReporter
{
    partial class report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.RepButton = new System.Windows.Forms.Button();
            this.filepath = new System.Windows.Forms.TextBox();
            this.url = new System.Windows.Forms.TextBox();
            this.author = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lab2 = new System.Windows.Forms.Label();
            this.lab1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.project = new System.Windows.Forms.TextBox();
            this.classtxt = new System.Windows.Forms.TextBox();
            this.code = new System.Windows.Forms.TextBox();
            this.desc = new System.Windows.Forms.TextBox();
            this.method = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.TextBox();
            this.backbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(217, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(294, 19);
            this.label5.TabIndex = 17;
            this.label5.Text = "Please fill in the below form to report a bug.";
            // 
            // RepButton
            // 
            this.RepButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.RepButton.Location = new System.Drawing.Point(575, 400);
            this.RepButton.Name = "RepButton";
            this.RepButton.Size = new System.Drawing.Size(125, 50);
            this.RepButton.TabIndex = 16;
            this.RepButton.Text = "Report";
            this.RepButton.UseVisualStyleBackColor = false;
            this.RepButton.Click += new System.EventHandler(this.RepButton_Click);
            // 
            // filepath
            // 
            this.filepath.Location = new System.Drawing.Point(187, 196);
            this.filepath.Name = "filepath";
            this.filepath.Size = new System.Drawing.Size(150, 20);
            this.filepath.TabIndex = 15;
            // 
            // url
            // 
            this.url.Location = new System.Drawing.Point(187, 148);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(150, 20);
            this.url.TabIndex = 14;
            // 
            // author
            // 
            this.author.Location = new System.Drawing.Point(187, 100);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(150, 20);
            this.author.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(49, 201);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "File Path";
            // 
            // lab2
            // 
            this.lab2.AutoSize = true;
            this.lab2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lab2.Location = new System.Drawing.Point(49, 155);
            this.lab2.Name = "lab2";
            this.lab2.Size = new System.Drawing.Size(115, 15);
            this.lab2.TabIndex = 11;
            this.lab2.Text = "Version Control URL";
            // 
            // lab1
            // 
            this.lab1.AutoSize = true;
            this.lab1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lab1.Location = new System.Drawing.Point(49, 105);
            this.lab1.Name = "lab1";
            this.lab1.Size = new System.Drawing.Size(44, 15);
            this.lab1.TabIndex = 10;
            this.lab1.Text = "Author";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(312, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 36);
            this.label1.TabIndex = 9;
            this.label1.Text = "Report";
            // 
            // project
            // 
            this.project.Location = new System.Drawing.Point(187, 246);
            this.project.Name = "project";
            this.project.Size = new System.Drawing.Size(150, 20);
            this.project.TabIndex = 18;
            // 
            // classtxt
            // 
            this.classtxt.Location = new System.Drawing.Point(187, 293);
            this.classtxt.Name = "classtxt";
            this.classtxt.Size = new System.Drawing.Size(150, 20);
            this.classtxt.TabIndex = 19;
            // 
            // code
            // 
            this.code.Location = new System.Drawing.Point(535, 100);
            this.code.Multiline = true;
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(150, 100);
            this.code.TabIndex = 20;
            // 
            // desc
            // 
            this.desc.Location = new System.Drawing.Point(535, 213);
            this.desc.Multiline = true;
            this.desc.Name = "desc";
            this.desc.Size = new System.Drawing.Size(150, 100);
            this.desc.TabIndex = 21;
            this.desc.Text = " ";
            // 
            // method
            // 
            this.method.Location = new System.Drawing.Point(187, 343);
            this.method.Name = "method";
            this.method.Size = new System.Drawing.Size(150, 20);
            this.method.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(49, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 15);
            this.label2.TabIndex = 23;
            this.label2.Text = "Project";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(49, 298);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 24;
            this.label3.Text = "Class";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(49, 343);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 15);
            this.label6.TabIndex = 25;
            this.label6.Text = "Method";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(424, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 15);
            this.label7.TabIndex = 26;
            this.label7.Text = "Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(424, 213);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 15);
            this.label8.TabIndex = 27;
            this.label8.Text = "Description";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(424, 343);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 15);
            this.label9.TabIndex = 28;
            this.label9.Text = "Date (DD/MM/YY)";
            // 
            // date
            // 
            this.date.Location = new System.Drawing.Point(535, 341);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(150, 20);
            this.date.TabIndex = 29;
            // 
            // backbutton
            // 
            this.backbutton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.backbutton.Location = new System.Drawing.Point(39, 400);
            this.backbutton.Name = "backbutton";
            this.backbutton.Size = new System.Drawing.Size(125, 50);
            this.backbutton.TabIndex = 30;
            this.backbutton.Text = "Back";
            this.backbutton.UseVisualStyleBackColor = false;
            this.backbutton.Click += new System.EventHandler(this.backbutton_Click);
            // 
            // report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(734, 462);
            this.Controls.Add(this.backbutton);
            this.Controls.Add(this.date);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.method);
            this.Controls.Add(this.desc);
            this.Controls.Add(this.code);
            this.Controls.Add(this.classtxt);
            this.Controls.Add(this.project);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.RepButton);
            this.Controls.Add(this.filepath);
            this.Controls.Add(this.url);
            this.Controls.Add(this.author);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lab2);
            this.Controls.Add(this.lab1);
            this.Controls.Add(this.label1);
            this.Name = "report";
            this.Text = "report";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button RepButton;
        private System.Windows.Forms.TextBox filepath;
        private System.Windows.Forms.TextBox url;
        private System.Windows.Forms.TextBox author;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lab2;
        private System.Windows.Forms.Label lab1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox project;
        private System.Windows.Forms.TextBox classtxt;
        private System.Windows.Forms.TextBox code;
        private System.Windows.Forms.TextBox desc;
        private System.Windows.Forms.TextBox method;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox date;
        private System.Windows.Forms.Button backbutton;
    }
}