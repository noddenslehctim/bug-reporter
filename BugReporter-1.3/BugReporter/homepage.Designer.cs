﻿namespace BugReporter
{
    partial class homepage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.repbugbutt = new System.Windows.Forms.Button();
            this.viewbugsbutt = new System.Windows.Forms.Button();
            this.urbugsbutt = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.fixedbutt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(228, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 59);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Reporter";
            // 
            // repbugbutt
            // 
            this.repbugbutt.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.repbugbutt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.repbugbutt.Location = new System.Drawing.Point(64, 164);
            this.repbugbutt.Name = "repbugbutt";
            this.repbugbutt.Size = new System.Drawing.Size(125, 125);
            this.repbugbutt.TabIndex = 1;
            this.repbugbutt.Text = "Report Bug";
            this.repbugbutt.UseVisualStyleBackColor = false;
            this.repbugbutt.Click += new System.EventHandler(this.repbugbutt_Click);
            // 
            // viewbugsbutt
            // 
            this.viewbugsbutt.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.viewbugsbutt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewbugsbutt.Location = new System.Drawing.Point(226, 164);
            this.viewbugsbutt.Name = "viewbugsbutt";
            this.viewbugsbutt.Size = new System.Drawing.Size(125, 125);
            this.viewbugsbutt.TabIndex = 2;
            this.viewbugsbutt.Text = "View Bugs";
            this.viewbugsbutt.UseVisualStyleBackColor = false;
            this.viewbugsbutt.Click += new System.EventHandler(this.viewbugsbutt_Click);
            // 
            // urbugsbutt
            // 
            this.urbugsbutt.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.urbugsbutt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urbugsbutt.Location = new System.Drawing.Point(391, 164);
            this.urbugsbutt.Name = "urbugsbutt";
            this.urbugsbutt.Size = new System.Drawing.Size(125, 125);
            this.urbugsbutt.TabIndex = 3;
            this.urbugsbutt.Text = "Your Bugs";
            this.urbugsbutt.UseVisualStyleBackColor = false;
            this.urbugsbutt.Click += new System.EventHandler(this.urbugsbutt_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(246, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(238, 26);
            this.label2.TabIndex = 4;
            this.label2.Text = "Welcome to Bug Reporter!";
            // 
            // fixedbutt
            // 
            this.fixedbutt.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.fixedbutt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fixedbutt.Location = new System.Drawing.Point(550, 164);
            this.fixedbutt.Name = "fixedbutt";
            this.fixedbutt.Size = new System.Drawing.Size(125, 125);
            this.fixedbutt.TabIndex = 5;
            this.fixedbutt.Text = "Fixed Bugs";
            this.fixedbutt.UseVisualStyleBackColor = false;
            this.fixedbutt.Click += new System.EventHandler(this.fixedbutt_Click);
            // 
            // homepage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(734, 462);
            this.Controls.Add(this.fixedbutt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.urbugsbutt);
            this.Controls.Add(this.viewbugsbutt);
            this.Controls.Add(this.repbugbutt);
            this.Controls.Add(this.label1);
            this.Name = "homepage";
            this.Text = "homepage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button repbugbutt;
        private System.Windows.Forms.Button viewbugsbutt;
        private System.Windows.Forms.Button urbugsbutt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button fixedbutt;
    }
}